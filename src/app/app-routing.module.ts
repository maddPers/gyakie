import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontPageComponent} from './front-page/front-page.component'

const routes: Routes = [  { path: 'gyakie', component: FrontPageComponent },
{ path: '**', pathMatch: 'full', redirectTo: 'gyakie' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
